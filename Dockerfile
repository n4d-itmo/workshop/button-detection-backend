# syntax = docker/dockerfile:1.4

FROM python:3.9

ARG MW_RESNET_GDRIVE_ID=1DbMuDaTLyunIsczdFJ8dDbG7_lkr5eNN

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_HOME='/usr/local'

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y

WORKDIR /app

RUN curl -sSL https://install.python-poetry.org | python - 
ENV PATH="/root/.local/bin:$PATH"

RUN poetry --version

COPY poetry.lock pyproject.toml ./

RUN poetry install

COPY . .

RUN make data

CMD poetry run fastapi run backend/main.py