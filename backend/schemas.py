from pydantic import BaseModel
from typing import Optional

class UserCreate(BaseModel):
    username: str
    email: str
    password: str

class User(BaseModel):
    id: int
    username: str
    email: str

    class Config:
        orm_mode = True

class Token(BaseModel):
    access_token: str
    token_type: str

class ProjectCreate(BaseModel):
    title: str
    description: Optional[str] = None

class Project(BaseModel):
    id: int
    title: str
    description: Optional[str]
    owner_id: int

    class Config:
        orm_mode = True

class ElementCreate(BaseModel):
    name: str
    blob_data: Optional[str] = None

class Element(BaseModel):
    id: int
    name: str
    blob_data: Optional[str] = None
    project_id: int
    picture: Optional[bytes] = None

    class Config:
        orm_mode = True