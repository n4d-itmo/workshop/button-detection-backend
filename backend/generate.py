from src.ml_part.button_analyzer.ButtonAnalyzer import ButtonAnalyzer
from fastapi import File, HTTPException
from pathlib import Path

b = ButtonAnalyzer()

def generate(filePath):
    return b.analyze_buttons_from(filePath)


def saveFile(file_bytes: bytes, filename: str):
    upload_dir = Path("uploaded_files")
    upload_dir.mkdir(parents=True, exist_ok=True)

    # Create the full path for the uploaded file
    file_path = upload_dir / filename

    # Save the uploaded file to the specified path
    try:
        with file_path.open("wb") as buffer:
            buffer.write(file_bytes)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to save file: {str(e)}")
    
    return file_path