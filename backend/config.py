from pydantic_settings import BaseSettings, SettingsConfigDict
from dotenv import dotenv_values

config = dotenv_values()


class Settings(BaseSettings):
    model_config = SettingsConfigDict(
        env_file=".env", env_file_encoding="utf-8", extra="ignore"
    )

    port: int = 8000
    database_uri: str = "postgresql://epvolyanitsa:password@localhost/postgres"


settings = Settings()