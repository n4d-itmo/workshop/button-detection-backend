from fastapi import Depends, FastAPI, HTTPException, status, File, UploadFile, Form
from sqlalchemy.orm import Session
from typing import List
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.middleware.cors import CORSMiddleware
from datetime import timedelta
import uvicorn
import base64
import json

from . import crud, models, schemas, auth
from .database import engine
from .dependencies import get_db
from .auth import get_current_user
from .config import settings
from .generate import generate, saveFile

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/register/", response_model=schemas.User)
def register_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)

@app.post("/token/", response_model=schemas.Token)
def login_for_access_token(db: Session = Depends(get_db), form_data: OAuth2PasswordRequestForm = Depends()):
    user = auth.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = auth.create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

@app.get("/users/me/", response_model=schemas.User)
def read_users_me(current_user: schemas.User = Depends(get_current_user)):
    return current_user

@app.post("/projects/", response_model=schemas.Project)
def create_project(
    project: schemas.ProjectCreate, 
    db: Session = Depends(get_db), 
    current_user: schemas.User = Depends(auth.get_current_user)
):
    return crud.create_project(db=db, project=project, user_id=current_user.id)


@app.delete("/projects/{project_id}/")
def delete_project(
    project_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(auth.get_current_user),
):
    return crud.delete_project(db=db, project_id=project_id, user_id=current_user.id)


@app.get("/projects/", response_model=List[schemas.Project])
def read_projects(
    db: Session = Depends(get_db), 
    current_user: schemas.User = Depends(auth.get_current_user)
):
    return crud.get_projects(db=db, user_id=current_user.id)

@app.post("/projects/{project_id}/elements/", response_model=schemas.Element)
async def create_element(
    project_id: int,
    name: str = Form(...),
    file: UploadFile = File(...),
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(auth.get_current_user)
):
    project = db.query(models.Project).filter(models.Project.id == project_id, models.Project.owner_id == current_user.id).first()
    if project is None:
        raise HTTPException(status_code=404, detail="Project not found")
    data = await file.read()
    rv = base64.b64encode(data)
    file_path = saveFile(data, file.filename)
    token = json.dumps(generate(file_path))
    element = schemas.ElementCreate(name=name, blob_data=token)
    return crud.create_element(db=db, element=element, project_id=project.id, picture=rv)

@app.get("/projects/{project_id}/elements/", response_model=List[schemas.Element])
def read_elements(
    project_id: int, 
    db: Session = Depends(get_db), 
    current_user: schemas.User = Depends(auth.get_current_user)
):
    project = db.query(models.Project).filter(models.Project.id == project_id, models.Project.owner_id == current_user.id).first()
    if project is None:
        raise HTTPException(status_code=404, detail="Project not found")
    return crud.get_elements(db=db, project_id=project.id)

@app.get("/projects/get_all_elements/", response_model=List[schemas.Element])
def read_all_elements(
    db: Session = Depends(get_db), 
    current_user: schemas.User = Depends(auth.get_current_user)
):
    projects = db.query(models.Project).filter(models.Project.owner_id == current_user.id).all()
    if projects is None:
        raise HTTPException(status_code=404, detail="Project not found")
    elements = []
    for project in projects:
        project_elements = db.query(models.Element).filter(models.Element.project_id == project.id).all()
        elements.extend(project_elements)
    
    return elements

def run():
    uvicorn.run("backend.main:app", host="0.0.0.0", port=settings.port, reload=True)

if __name__ == "__main__":
    run()
