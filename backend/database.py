from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from databases import Database

from .config import settings

DATABASE_URL = settings.database_uri

engine = create_engine(DATABASE_URL)
metadata = MetaData()
database = Database(DATABASE_URL)
Base = declarative_base()
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)