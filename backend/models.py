from sqlalchemy import Column, ForeignKey, Integer, String, LargeBinary, Text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    projects = relationship("Project", back_populates="owner")

class Project(Base):
    __tablename__ = "projects"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(Text)
    owner_id = Column(Integer, ForeignKey('users.id'))
    owner = relationship("User", back_populates="projects")
    elements = relationship("Element", back_populates="project")

class Element(Base):
    __tablename__ = "elements"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    picture = Column(LargeBinary)
    blob_data = Column(Text)
    project_id = Column(Integer, ForeignKey('projects.id'))
    project = relationship("Project", back_populates="elements")