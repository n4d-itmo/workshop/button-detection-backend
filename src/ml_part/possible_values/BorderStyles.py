from enum import Enum


class BorderStyles(Enum):
    NONE = 'none'
    SOLID = 'solid'
    DOTTED = 'dotted'
    DOUBLE = 'double'
    GROOVE = 'groove'
    RIDGE = 'ridge'
    INSET = 'inset'
    OUTSET = 'outset'


class LabelsBorderStyles(Enum):
    NONE = 0
    SOLID = 1
    DOTTED = 2
    DOUBLE = 3
    GROOVE = 4
    RIDGE = 5
    INSET = 6
    OUTSET = 7
