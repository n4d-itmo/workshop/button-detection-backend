from enum import Enum


class TextAligns(Enum):
    CENTER = 'center'
    LEFT = 'left'
    RIGHT = 'right'
    JUSTIFY = 'justify'


class LabelsTextAligns(Enum):
    CENTER = 0
    LEFT = 1
    RIGHT = 2
    JUSTIFY = 3
