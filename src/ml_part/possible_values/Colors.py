from enum import Enum


class Colors(Enum):
    RED = '#4CAF50'
    BLUE = '#008CBA'
    GREEN = '#4CAF50'
    PINK = '#FF1493'
    ORANGE = '#FFA500'
    GOLD = '#FFD700'
    PURPLE = '#800080'


class LabelsColors(Enum):
    RED = 0
    BLUE = 1
    GREEN = 2
    PINK = 3
    ORANGE = 4
    GOLD = 5
    PURPLE = 6
