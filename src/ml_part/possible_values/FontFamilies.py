from enum import Enum


class FontFamilies(Enum):
    TIMES_NEW_ROMAN = 'Times New Roman'
    ARIAL = 'Arial'


class LabelsFontFamilies(Enum):
    TIMES_NEW_ROMAN = 0
    ARIAL = 1
