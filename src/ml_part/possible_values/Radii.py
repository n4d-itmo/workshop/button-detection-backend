from enum import Enum


class Radii(Enum):
    RECTANGLE = '0px'
    ELLIPSIS = '20px/10px'
    CIRCLE = '100000px'
