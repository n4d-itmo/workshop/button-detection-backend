from enum import Enum


class FontStyles(Enum):
    NORMAL = 'normal'
    ITALIC = 'italic'
    OBLIQUE = 'oblique'


class LabelsFontStyles(Enum):
    NORMAL = 0
    ITALIC = 1
    OBLIQUE = 2
